baseURL = "https://janmasrovira.gitlab.io/ascetic-slug/"
languageCode = "en"
defaultContentLanguage = "en"
title = "Ascetig slug"
preserveTaxonomyNames = true
enableRobotsTXT = true
enableEmoji = true
theme = "even"

pygmentsOptions = ""
pygmentsCodefences = true
pygmentsUseClasses = true
pygmentsCodefencesGuessSyntax = true

hasCJKLanguage = false
paginate = 3
disqusShortname = "ascetic-slug"
googleAnalytics = "UA-114512487-1"
copyright = ""

[markup.goldmark.renderer]
  unsafe = true

[author]
  name = "Jan Mas Rovira"

[sitemap]
  changefreq = "weekly"
  priority = 0.5
  filename = "sitemap.xml"

[[menu.main]]
  name = "Home"
  weight = 10
  identifier = "home"
  url = "/"
[[menu.main]]
  name = "Tags"
  weight = 30
  identifier = "tags"
  url = "/tags/"
[[menu.main]]
  name = "Archives"
  weight = 20
  identifier = "archives"
  url = "/post/"

[params]
  version = "4.x"
  debug = false

  since = "2018"

  logoTitle = "Ascetic slug"
  keywords = ["Programming", "Computer", "Science", "Haskell", "Logic", "Agda"]
  description = "Programming and logic"

  archivePaginate = 5

  # show 'xx Posts In Total' in archive page?
  showArchiveCount = false

  dateFormatToUse = "Monday, January 2, 2006"

  # show word count and read time ?
  moreMeta = false

  # Syntax highlighting by highlight.js
  highlightInClient = false

  # Syntax highlighting by Chroma. NOTE: Don't enable `highlightInClient` and `chroma` at the same time!
  pygmentsOptions = "linenos=table"
  pygmentsCodefences = true
  pygmentsUseClasses = true
  pygmentsCodefencesGuessSyntax = true


  toc = true
  autoCollapseToc = false   # Auto expand and collapse toc
  fancybox = true           # see https://github.com/fancyapps/fancybox
  mathjax = true            # see https://www.mathjax.org/
  mathjaxEnableSingleDollar = true # $...$

  postMetaInFooter = true   # contain author, lastMod, markdown link, license
  linkToMarkDown = false    # Only effective when hugo will output .md files.
  contentCopyright = ''     # e.g. '<a rel="license noopener" href="https://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank">CC BY-NC-ND 4.0</a>'

  changyanAppid = ""        # Changyan app id
  changyanAppkey = ""       # Changyan app key
  livereUID = ""            # LiveRe UID
  baidu_push = false        # baidu push
  baidu_analytics = ""      # Baidu Analytics
  baidu_verification = ""   # Baidu Verification
  google_verification = ""  # Google_Verification

  # Link custom CSS and JS assets
  #   (relative to /static/css and /static/js respectively)
  customCSS = []
  customJS = []

[params.publicCDN]        # load these files from public cdn
    enable = true
    jquery = '<script src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>'
    slideout = '<script src="https://cdn.jsdelivr.net/npm/slideout@1.0.1/dist/slideout.min.js" integrity="sha256-t+zJ/g8/KXIJMjSVQdnibt4dlaDxc9zXr/9oNPeWqdg=" crossorigin="anonymous"></script>'
    fancyboxJS = '<script src="https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.1.20/dist/jquery.fancybox.min.js" integrity="sha256-XVLffZaxoWfGUEbdzuLi7pwaUJv1cecsQJQqGLe7axY=" crossorigin="anonymous"></script>'
    fancyboxCSS = '<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.1.20/dist/jquery.fancybox.min.css" integrity="sha256-7TyXnr2YU040zfSP+rEcz29ggW4j56/ujTPwjMzyqFY=" crossorigin="anonymous">'
    timeagoJS = '<script src="https://cdn.jsdelivr.net/npm/timeago.js@3.0.2/dist/timeago.min.js" integrity="sha256-jwCP0NAdCBloaIWTWHmW4i3snUNMHUNO+jr9rYd2iOI=" crossorigin="anonymous"></script>'
    timeagoLocalesJS = '<script src="https://cdn.jsdelivr.net/npm/timeago.js@3.0.2/dist/timeago.locales.min.js" integrity="sha256-ZwofwC1Lf/faQCzN7nZtfijVV6hSwxjQMwXL4gn9qU8=" crossorigin="anonymous"></script>'
    flowchartDiagramsJS = '<script src="https://cdn.jsdelivr.net/npm/raphael@2.2.7/raphael.min.js" integrity="sha256-67By+NpOtm9ka1R6xpUefeGOY8kWWHHRAKlvaTJ7ONI=" crossorigin="anonymous"></script> <script src="https://cdn.jsdelivr.net/npm/flowchart.js@1.8.0/release/flowchart.min.js" integrity="sha256-zNGWjubXoY6rb5MnmpBNefO0RgoVYfle9p0tvOQM+6k=" crossorigin="anonymous"></script>'
    sequenceDiagramsCSS = '<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bramp/js-sequence-diagrams@2.0.1/dist/sequence-diagram-min.css" integrity="sha384-6QbLKJMz5dS3adWSeINZe74uSydBGFbnzaAYmp+tKyq60S7H2p6V7g1TysM5lAaF" crossorigin="anonymous">'
    sequenceDiagramsJS = '<script src="https://cdn.jsdelivr.net/npm/webfontloader@1.6.28/webfontloader.js" integrity="sha256-4O4pS1SH31ZqrSO2A/2QJTVjTPqVe+jnYgOWUVr7EEc=" crossorigin="anonymous"></script> <script src="https://cdn.jsdelivr.net/npm/snapsvg@0.5.1/dist/snap.svg-min.js" integrity="sha256-oI+elz+sIm+jpn8F/qEspKoKveTc5uKeFHNNVexe6d8=" crossorigin="anonymous"></script> <script src="https://cdn.jsdelivr.net/npm/underscore@1.8.3/underscore-min.js" integrity="sha256-obZACiHd7gkOk9iIL/pimWMTJ4W/pBsKu+oZnSeBIek=" crossorigin="anonymous"></script> <script src="https://cdn.jsdelivr.net/gh/bramp/js-sequence-diagrams@2.0.1/dist/sequence-diagram-min.js" integrity="sha384-8748Vn52gHJYJI0XEuPB2QlPVNUkJlJn9tHqKec6J3q2r9l8fvRxrgn/E5ZHV0sP" crossorigin="anonymous"></script>'

[params.sequenceDiagrams]
  enable = true
  options = "{theme: 'hand'}"

[params.busuanzi]         # count web traffic by busuanzi
  enable = false
  siteUV = true
  sitePV = true
  pagePV = true

[params.social]
  a-email = "mailto:janmasrovira@gmail.com"
  g-github = "https://github.com/janmasrovira"
  n-gitlab = "https://gitlab.com/janmasrovira"
